'use strict'
const fs = require('fs');
const api = require('./api/index');

api('./data/').then(console.log);

const express = require('express');

const app = express();

const port = 3000

app.set('view engine', 'pug');

app.get('/', async(req, res) => {
    const fileName = await api('./data/');
    fs.readFile(fileName, (err, data) => {
        if (err) throw err;
        let api = JSON.parse(data);
        res.render('index', { title: 'tretton37', message: 'The fellowship of the tretton37', colleagues: api.colleagues, office: "Office Lund" });
    });
});

app.listen(port, () => console.log(`App listening on port ${port}`));