const scrapeIt = require("scrape-it");
const fs = require('fs');

const fileName = "colleagues.json";
const address = "https://tretton37.com/meet";

/**
 * Get filename of json object saved to disk
 * @return Promise
 */
const scrapeAndSave = (path, ms) => {
    return new Promise((resolve, reject) => {
        scrapeIt(address, {
            colleagues: {
                listItem: ".ninja-summary",
                data: {
                    name: "h1",
                    socials: {
                        listItem: ".social-icons > a",
                        data: {
                            social: {
                                attr: "href"
                            }
                        }
                    },
                    portrait: {
                        selector: ".portrait",
                        attr: "src"
                    }
                }
            }
        }).then(({ data, response }) => {
            console.log(`Status Code: ${response.statusCode}`);
            data.made = ms;
            fs.writeFile(path + fileName, JSON.stringify(data), 'utf8', (err) => {
                if (err) {
                    console.log("An error occured while writing JSON Object to " + fileName);
                    return console.log(err);
                }
                console.log(fileName + " has been saved.");
                return resolve(path + fileName);
            });
        }).catch(e => reject(e));
    });
}

/**
 * Scrapes for data and returns new data if the old > 1h
 * @returns Promise
 */
module.exports = (path) => {

    const date = new Date();
    const ms = date.getTime();

    return new Promise((resolve, reject) => {

        fs.readFile(path + fileName, 'utf8', (err, jsonString) => {
            // File exists
            if (!err) {
                let obj = JSON.parse(jsonString);
                // We have data withing an hour skip collecting new and return
                if ((obj.made - ms) < (3.6 * 10 ** 6)) {
                    return resolve(path + fileName);
                } else {
                    return scrapeAndSave(path, ms).then(resolve).catch(reject);
                }
            }
            // We don't have a file create a new
            return scrapeAndSave(path, ms).then(resolve).catch(reject);
        })
    })
};